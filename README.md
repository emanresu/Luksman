# Luksman

![image alt text](screenshot.png "luksman screenshot")

A simple LUKS container manager through the CLI.

## Install

>chmod +x install.sh

>chmod +x luksman

>sudo sh install.sh

## Uninstall

>chmod +x uninstall.sh

>sudo sh uninstall.sh

## Usage

Script can be ran by simply typing "luksman" after installation.
